import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListCarComponent } from './carmodule/components/list-car/list-car.component';
import { AddCarComponent } from './carmodule/components/add-car/add-car.component';
import { ViewCarComponent } from './carmodule/components/view-car/view-car.component';
import { UpdateCarComponent } from './carmodule/components/update-car/update-car.component';


const routes: Routes = [
  {
  //   path: 'list-car',
  //   children: [
  //     {
  //       path: '',
  //       component: ListCarComponent
  //     },
  //     {
  //       path: 'create',
  //       component: AddCarComponent
  //     }
  //   ]
  path: 'list-car',
  component: ListCarComponent },
  {
    path: 'list-car/create',
    children: [
      {
        path: '',
        component: AddCarComponent
      },
      {
        path: 'list-car/:id',
        component: ViewCarComponent
      }
    ]
  },
  {
    path: 'list-car/update',
    component: UpdateCarComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
