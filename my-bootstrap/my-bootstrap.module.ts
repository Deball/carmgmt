import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WavesModule, TableModule, ButtonsModule, IconsModule, InputsModule } from 'angular-bootstrap-md';


const appShareModule = [
  CommonModule,
  WavesModule,
  TableModule,
  ButtonsModule,
  IconsModule,
  InputsModule
];

@NgModule({
  declarations: [],
  imports: appShareModule,
  exports: appShareModule
})
export class MyBootstrapModule { }
