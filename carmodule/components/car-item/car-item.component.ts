import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
// import { Car } from '../../model/car.interface';

@Component({
  selector: 'app-car-item',
  templateUrl: './car-item.component.html',
  styleUrls: ['./car-item.component.scss']
})
export class CarItemComponent implements OnInit {

  // @Input() aCar: Car;
  // @Output() removeCarAction: EventEmitter<Car> = new EventEmitter();
  // @Output() updateCarAction: EventEmitter<Car> = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }

  // removeCar() {
  //   this.removeCarAction.emit(this.aCar);
  // }

  // updateCar(){
  //   this.updateCarAction.emit(this.aCar);
  // }
}
