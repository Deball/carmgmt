import { Component, OnInit, EventEmitter } from '@angular/core';
import { Car } from '../../model/car.interface';

@Component({
  selector: 'app-list-car',
  templateUrl: './list-car.component.html',
  styleUrls: ['./list-car.component.scss']
})
export class ListCarComponent implements OnInit {

  cars: Car[] = [];
  headElements = ['MAKE', 'ENGINE', 'COLOR', 'MODEL', 'YEAR', 'ACTIONS'];
  editableCar: EventEmitter<Car> = new EventEmitter(null);

  constructor() { }

  ngOnInit() {
    // get the cars from the localstorage
    this.cars = JSON.parse(localStorage.getItem('cars') || '[]');
  }

  addCar(car: Car) {
    console.log('You click the add button', car);
    this.cars.push(car);

    // persist it in the local storage
    localStorage.setItem('cars', JSON.stringify(this.cars));
  }

  removeCar(car: Car) {
    console.log('You clicked remove car');
    let index = this.cars.indexOf(car);
    console.log('index: ', index);
    this.cars.splice(index, 1);

    // persist it
    localStorage.setItem('cars', JSON.stringify(this.cars));
  }

  updateCar(car: Car){
    console.log('You clicked the update car button');
    this.editableCar.next(car);

  }

}
