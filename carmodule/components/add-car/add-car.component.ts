import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Car } from 'src/app/carmodule/model/car.interface';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-add-car',
  templateUrl: './add-car.component.html',
  styleUrls: ['./add-car.component.scss']
})
export class AddCarComponent implements OnInit {

  @Output() addCar: EventEmitter<Car> = new EventEmitter();
  @Input() parentcar: EventEmitter<Car> = new EventEmitter(null);

  addCarForm: FormGroup;

  constructor(private router: Router, private fb: FormBuilder) { }

  ngOnInit() {
    // this.addCarForm = this.fb.group({
    //   make: [this.parentcar && this.parentcar.make ? this.parentcar.make : '', [Validators.minLength(3), Validators.required]],
    //   engine: [this.parentcar && this.parentcar.engine ? this.parentcar.engine : '', [Validators.minLength(2)]],
    //   color: [this.parentcar && this.parentcar.color ? this.parentcar.color : '', [Validators.minLength(3), Validators.required]],
    //   model: [this.parentcar && this.parentcar.model ? this.parentcar.model : '', [Validators.minLength(4), Validators.required]],
    //   year: [this.parentcar && this.parentcar.year ? this.parentcar.year : '', [Validators.minLength(2), Validators.required]]
    // });
    this.addCarForm = this.fb.group({
      make: ['', [Validators.minLength(3), Validators.required]],
      engine: ['', [Validators.minLength(2)]],
      color: ['', [Validators.minLength(3), Validators.required]],
      model: ['', [Validators.minLength(4), Validators.required]],
      year: ['', [Validators.minLength(2), Validators.required]]
    });

    this.parentcar.subscribe(car => this.prepopulateForm(car));
  }

  // To populate the form with data
  prepopulateForm(car: Car) {
    this.addCarForm.setValue(car);
  }

  /*function to add a car*/
  addCarAction() {
    if (this.addCarForm.valid) {
      this.addCar.emit(this.addCarForm.value);
      this.addCarForm.reset();
    }
  }

  /*function to cancel the addCar operation*/
  cancelCarAction() {
    this.router.navigate(['/listCar']);
  }

  /*function to clear form*/
  clearForm() {
    this.addCarForm.reset();
  }



}
