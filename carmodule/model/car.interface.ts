export interface Car {
  make: string;
  engine: string;
  year: string;
  model: string;
  color: string;
}
