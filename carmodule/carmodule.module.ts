import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ListCarComponent } from './components/list-car/list-car.component';
import { AddCarComponent } from './components/add-car/add-car.component';
import { UpdateCarComponent } from './components/update-car/update-car.component';
import { ViewCarComponent } from './components/view-car/view-car.component';
import { CarItemComponent } from './components/car-item/car-item.component';
import { MyBootstrapModule } from '../my-bootstrap/my-bootstrap.module';


const appShareComponent = [
  ListCarComponent,
  AddCarComponent,
  UpdateCarComponent,
  ViewCarComponent,
  CarItemComponent
];

const appShareModule = [
  CommonModule,
  ReactiveFormsModule,
  FormsModule,
  MyBootstrapModule
];

@NgModule({
  declarations: appShareComponent,
  imports: appShareModule,
  exports: appShareComponent
})
export class CarmoduleModule { }
